package org.mytests.tests.Smoke;

import org.mytests.tests.SimpleTestsInit;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.testng.Assert;
import org.testng.annotations.Test;

import static org.mytests.uiobjects.site.AutomationPracticeSite.homePage;
import static org.mytests.uiobjects.site.AutomationPracticeSite.loginPage;
import static org.mytests.uiobjects.site.pages.AccountPage.buttonWithName;
import static org.mytests.uiobjects.site.pages.CartPage.checkPriceElement;
import static org.mytests.uiobjects.site.pages.CartPage.shipmentPriceElement;
import static org.mytests.uiobjects.site.pages.HomePage.*;
import static org.mytests.uiobjects.site.pages.LoginPage.*;

public class CartTests implements SimpleTestsInit {
    @Test
    public void addProductFromSearchIntoCart(){
        String searchWord = "dress";
        homePage.open();
        searchField.input(searchWord);
        searchField.sendKeys(Keys.ENTER);
        productDescriptions.get(0).hover();
        float productPrice = Float.parseFloat(productPriceElement.getText().substring(1));
        addToCartButton.click();
        continueShopping.click();
        cartButton.click();
        //cartPage.isOpened();
        float shipmentPrice = Float.parseFloat(shipmentPriceElement.getText().substring(1));
        float checkPrice = Float.parseFloat(checkPriceElement.getText().substring(1));
        Assert.assertEquals(productPrice+shipmentPrice,checkPrice);
    }
}
