package org.mytests.tests.Smoke;

import org.mytests.tests.SimpleTestsInit;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.testng.Assert;
import org.testng.annotations.Test;

import static org.mytests.uiobjects.site.AutomationPracticeSite.homePage;
import static org.mytests.uiobjects.site.AutomationPracticeSite.loginPage;
import static org.mytests.uiobjects.site.pages.AccountPage.buttonWithName;
import static org.mytests.uiobjects.site.pages.CartPage.checkPriceElement;
import static org.mytests.uiobjects.site.pages.CartPage.shipmentPriceElement;
import static org.mytests.uiobjects.site.pages.HomePage.*;
import static org.mytests.uiobjects.site.pages.LoginPage.*;

public class SearchTests implements SimpleTestsInit {
    @Test
    public void positiveSearchUsingButton(){
        String searchWord = "dress";
        homePage.open();
        searchField.input(searchWord);
        searchButton.click();
        boolean k = false;
        for (WebElement productName : productNames)
        {
            if (productName.getText().toLowerCase().contains(searchWord.toLowerCase())){
                k = true;
                break;
            }
        }
        Assert.assertTrue(k);
    }
    @Test
    public void positiveSearchUsingEnter(){
        String searchWord = "dress";
        homePage.open();
        searchField.input(searchWord);
        searchField.sendKeys(Keys.ENTER);
        boolean k = false;
        for (WebElement productName : productNames)
        {
            if (productName.getText().toLowerCase().contains(searchWord.toLowerCase())){
                k = true;
                break;
            }
        }
        Assert.assertTrue(k);
    }
}
