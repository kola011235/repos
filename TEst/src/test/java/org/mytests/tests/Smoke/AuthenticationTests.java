package org.mytests.tests.Smoke;

import org.mytests.tests.SimpleTestsInit;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.testng.Assert;
import org.testng.annotations.Test;

import static org.mytests.uiobjects.site.AutomationPracticeSite.homePage;
import static org.mytests.uiobjects.site.AutomationPracticeSite.loginPage;
import static org.mytests.uiobjects.site.pages.AccountPage.buttonWithName;
import static org.mytests.uiobjects.site.pages.CartPage.checkPriceElement;
import static org.mytests.uiobjects.site.pages.CartPage.shipmentPriceElement;
import static org.mytests.uiobjects.site.pages.HomePage.*;
import static org.mytests.uiobjects.site.pages.LoginPage.*;

public class AuthenticationTests implements SimpleTestsInit {
    @Test
    public void positiveLogin(){
        String positiveUserName = "pinkpie011235@gmail.com";
        String positivePassword = "password";
        String positiveName = "Name";
        String positiveSurname = "Surname";
        homePage.open();
        loginButton.click();
        loginPage.isOpened();
        userName.input(positiveUserName);
        password.input(positivePassword);
        enter.click();
        Assert.assertEquals(buttonWithName.firstChild().getLines().get(0), positiveName + " " + positiveSurname);
    }
}
