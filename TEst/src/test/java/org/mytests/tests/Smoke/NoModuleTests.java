package org.mytests.tests.Smoke;

import org.mytests.tests.SimpleTestsInit;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.testng.Assert;
import org.testng.annotations.Test;

import static org.mytests.uiobjects.site.AutomationPracticeSite.homePage;
import static org.mytests.uiobjects.site.AutomationPracticeSite.loginPage;
import static org.mytests.uiobjects.site.pages.AccountPage.buttonWithName;
import static org.mytests.uiobjects.site.pages.CartPage.checkPriceElement;
import static org.mytests.uiobjects.site.pages.CartPage.shipmentPriceElement;
import static org.mytests.uiobjects.site.pages.HomePage.*;
import static org.mytests.uiobjects.site.pages.LoginPage.*;

public class NoModuleTests implements SimpleTestsInit {
    static String siteUrl = "http://automationpractice.com/";
    @Test
    public void openTest() {
        homePage.open();
        Assert.assertTrue(loginButton.isDisplayed());
    }
}
