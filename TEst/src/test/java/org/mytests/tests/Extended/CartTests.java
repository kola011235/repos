package org.mytests.tests.Extended;

import org.mytests.tests.SimpleTestsInit;
import org.openqa.selenium.Keys;
import org.testng.Assert;
import org.testng.annotations.Test;

import static org.mytests.uiobjects.site.AutomationPracticeSite.cartPage;
import static org.mytests.uiobjects.site.AutomationPracticeSite.homePage;
import static org.mytests.uiobjects.site.pages.CartPage.checkPriceElement;
import static org.mytests.uiobjects.site.pages.CartPage.quantity;
import static org.mytests.uiobjects.site.pages.HomePage.*;

public class CartTests implements SimpleTestsInit {
    public void cartPreparation(){
        String searchWord = "dress";
        homePage.open();
        searchField.input(searchWord);
        searchField.sendKeys(Keys.ENTER);
        productDescriptions.get(0).hover();
        addToCartButton.click();
        continueShopping.click();
        cartPage.open();
    }
    @Test
    public void negativeInputsForQuantityInCart(){
        try {
            cartPreparation();
            float checkPrice = Float.parseFloat(checkPriceElement.getText().substring(1));
            quantity.input("string");
            quantity.sendKeys(Keys.ENTER);
            Thread.sleep(   1500);
            boolean firstCondition = (checkPrice == Float.parseFloat(checkPriceElement.getText().substring(1)));
            quantity.input("-1");
            quantity.sendKeys(Keys.ENTER);
            Thread.sleep(   1500);
            boolean secondCondition = (checkPrice == Float.parseFloat(checkPriceElement.getText().substring(1)));
            quantity.input("0");
            quantity.sendKeys(Keys.ENTER);
            Thread.sleep(   1500);
            boolean thirdCondition = (checkPrice == Float.parseFloat(checkPriceElement.getText().substring(1)));
            Assert.assertTrue(firstCondition && secondCondition && thirdCondition);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

    }
}
