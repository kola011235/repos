package org.mytests.tests.Extended;

import org.mytests.tests.SimpleTestsInit;
import org.openqa.selenium.Keys;
import org.testng.Assert;
import org.testng.annotations.Test;

import static org.mytests.uiobjects.site.AutomationPracticeSite.cartPage;
import static org.mytests.uiobjects.site.AutomationPracticeSite.homePage;
import static org.mytests.uiobjects.site.pages.CartPage.checkPriceElement;
import static org.mytests.uiobjects.site.pages.CartPage.quantity;
import static org.mytests.uiobjects.site.pages.HomePage.*;

public class SearchTests implements SimpleTestsInit {
    @Test
    public void noInputSearchUsingButton() {
        homePage.open();
        searchButton.click();
        String expectedMessage = "Please enter a search keyword";
        Assert.assertEquals(searchMessage.getText(), expectedMessage);
    }
}
