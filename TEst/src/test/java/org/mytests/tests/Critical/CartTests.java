package org.mytests.tests.Critical;

import com.epam.jdi.light.driver.WebDriverFactory;
import org.mytests.tests.SimpleTestsInit;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.Test;

import static org.mytests.uiobjects.site.AutomationPracticeSite.cartPage;
import static org.mytests.uiobjects.site.AutomationPracticeSite.loginPage;
import static org.mytests.uiobjects.site.pages.CartPage.*;
import static org.mytests.uiobjects.site.pages.HomePage.*;
import static org.mytests.uiobjects.site.pages.LoginPage.*;
import static org.mytests.uiobjects.site.pages.SummerDressPage.*;

public class CartTests implements SimpleTestsInit {
    @Test
    public void addProductFromProductPageIntoCart(){
        String searchWord = "dress";
        searchField.input(searchWord);
        searchField.sendKeys(Keys.ENTER);
        productDescriptions.get(0).hover();
        float productPrice = Float.parseFloat(productPriceElement.getText().substring(1));
        moreButton.click();

        addToCartSummerDressButton.click();
        continueShoppingSummerDressButton.click();
        cartButton.click();
        //cartPage.isOpened();
        float shipmentPrice = Float.parseFloat(shipmentPriceElement.getText().substring(1));
        float checkPrice = Float.parseFloat(checkPriceElement.getText().substring(1));
        Assert.assertEquals(productPrice+shipmentPrice,checkPrice);
    }
    public void cartPreparation(){
        String searchWord = "dress";
        searchField.input(searchWord);
        searchField.sendKeys(Keys.ENTER);
        productDescriptions.get(0).hover();
        addToCartButton.click();
        continueShopping.click();
        cartPage.open();
    }
    @Test
    public void deleteFromCart(){
        cartPreparation();
        trashButton.click();
        String expectedErrorMessage = "Your shopping cart is empty.";
        WebDriverWait wait = new WebDriverWait(WebDriverFactory.getDriver(), 5);
        wait.until(ExpectedConditions.invisibilityOf((WebElement)trashButton));
        Assert.assertEquals(errorMessage.getText(),expectedErrorMessage);
    }
    @Test
    public void changeAmountWithButtons(){
        try{
            cartPreparation();
            float productprice = Float.parseFloat(cartProductPrice.getText().substring((1)));
            float shipmentPrice = Float.parseFloat(shipmentPriceElement.getText().substring(1));
            plusButton.click();
            Thread.sleep(500);
            float checkPrice = Float.parseFloat(checkPriceElement.getText().substring(1));
            boolean firstCondition = (checkPrice == productprice*2+shipmentPrice);
            Thread.sleep(500);
            minusButton.click();
            checkPrice = Float.parseFloat(checkPriceElement.getText().substring(1));
            boolean secondCondition = (checkPrice == productprice+shipmentPrice);
            Thread.sleep(500);
            minusButton.click();
            Thread.sleep(500);
            float lastCheckPrice = Float.parseFloat(checkPriceElement.getText().substring(1));
            boolean thirdCondition = (checkPrice == lastCheckPrice);
            Assert.assertTrue(firstCondition && secondCondition && thirdCondition);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
    @Test
    public void changeAmountWithField(){
        cartPreparation();
        float productprice = Float.parseFloat(cartProductPrice.getText().substring((1)));
        float shipmentPrice = Float.parseFloat(shipmentPriceElement.getText().substring(1));
        quantity.input("10");
        quantity.sendKeys(Keys.ENTER);
        try {
            Thread.sleep(500);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        float checkPrice = Float.parseFloat(checkPriceElement.getText().substring(1));
        Assert.assertTrue((checkPrice == productprice*10+shipmentPrice));
    }
    @Test
    public void addThreeProductsFromProductPageIntoCart(){
        String searchWord = "dress";
        searchField.input(searchWord);
        searchField.sendKeys(Keys.ENTER);
        productDescriptions.get(0).hover();
        float productPrice = Float.parseFloat(productPriceElement.getText().substring(1));
        moreButton.click();
        dressQuantity.input("3");
        addToCartSummerDressButton.click();
        continueShoppingSummerDressButton.click();
        cartButton.click();
        float shipmentPrice = Float.parseFloat(shipmentPriceElement.getText().substring(1));
        float checkPrice = Float.parseFloat(checkPriceElement.getText().substring(1));
        Assert.assertEquals(productPrice*3+shipmentPrice,checkPrice);
    }
    @Test
    public void addProductWithDifferentColorFromProductPageIntoCart(){
        String searchWord = "dress";
        searchField.input(searchWord);
        searchField.sendKeys(Keys.ENTER);
        productDescriptions.get(0).hover();
        float productPrice = Float.parseFloat(productPriceElement.getText().substring(1));
        moreButton.click();
        blueColorButton.click();
        addToCartSummerDressButton.click();
        continueShoppingSummerDressButton.click();
        cartButton.click();
        System.out.println(colorInfo.getText());
        Assert.assertTrue(colorInfo.getText().toLowerCase().contains("blue"));
        //cartPage.isOpened();
    }
    @Test
    public void addThreeProductsFromSearchIntoCart(){
        String searchWord = "dress";
        searchField.input(searchWord);
        searchField.sendKeys(Keys.ENTER);
        for (int i = 0; i<3;i++){
            productDescriptions.get(0).hover();
            addToCartButton.click();
            continueShopping.click();
        }
        productDescriptions.get(0).hover();
        float productPrice = Float.parseFloat(productPriceElement.getText().substring(1));
        cartButton.click();
        float shipmentPrice = Float.parseFloat(shipmentPriceElement.getText().substring(1));
        float checkPrice = Float.parseFloat(checkPriceElement.getText().substring(1));
        Assert.assertEquals(productPrice*3+shipmentPrice,checkPrice);
    }
}
