package org.mytests.tests.Critical;

import com.epam.jdi.light.driver.WebDriverFactory;
import org.mytests.tests.SimpleTestsInit;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.Test;

import static org.mytests.uiobjects.site.AutomationPracticeSite.cartPage;
import static org.mytests.uiobjects.site.AutomationPracticeSite.loginPage;
import static org.mytests.uiobjects.site.pages.CartPage.*;
import static org.mytests.uiobjects.site.pages.HomePage.*;
import static org.mytests.uiobjects.site.pages.LoginPage.*;
import static org.mytests.uiobjects.site.pages.SummerDressPage.*;

public class AuthenticationTests implements SimpleTestsInit {
    @Test
    public void wrongPasswordAuthentication(){
        String positiveUserName = "pinkpie011235@gmail.com";
        String wrongPassword = "OH_WRONG_PASSWORD";
        String expectedMessage = "Authentication failed.";
        loginButton.click();
        loginPage.isOpened();
        userName.input(positiveUserName);
        password.input(wrongPassword);
        enter.click();
        Assert.assertEquals(firstPartOfAlarmMessage.getText(), expectedMessage);
    }
    @Test
    public void wrongLoginAuthentication(){
        String wrongUserName = "randommail@gmail.com";
        String positivePassword = "password";
        String expectedMessage = "Authentication failed.";
        loginButton.click();
        loginPage.isOpened();
        userName.input(wrongUserName);
        password.input(positivePassword);
        enter.click();
        Assert.assertEquals(firstPartOfAlarmMessage.getText(), expectedMessage);
    }
    @Test
    public void wrongLoginAndPasswordAuthentication(){
        String wrongUserName = "randommail@gmail.com";
        String wrongPassword = "OH_WRONG_PASSWORD";
        String expectedMessage = "Authentication failed.";
        loginButton.click();
        loginPage.isOpened();
        userName.input(wrongUserName);
        password.input(wrongPassword);
        enter.click();
        Assert.assertEquals(firstPartOfAlarmMessage.getText(), expectedMessage);
    }
}
