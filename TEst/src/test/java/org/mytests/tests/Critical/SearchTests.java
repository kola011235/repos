package org.mytests.tests.Critical;

import com.epam.jdi.light.driver.WebDriverFactory;
import org.mytests.tests.SimpleTestsInit;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.Test;

import static org.mytests.uiobjects.site.AutomationPracticeSite.cartPage;
import static org.mytests.uiobjects.site.AutomationPracticeSite.loginPage;
import static org.mytests.uiobjects.site.pages.CartPage.*;
import static org.mytests.uiobjects.site.pages.HomePage.*;
import static org.mytests.uiobjects.site.pages.LoginPage.*;
import static org.mytests.uiobjects.site.pages.SummerDressPage.*;

public class SearchTests implements SimpleTestsInit {
    @Test
    public void wrongWordSearchUsingEnter(){
        String wrongSearchWord = "qwertyQWERTY";
        searchField.input(wrongSearchWord);
        searchField.sendKeys(Keys.ENTER);
        Assert.assertTrue(searchMessage.getText().contains("No results were found for your search"));
    }
}