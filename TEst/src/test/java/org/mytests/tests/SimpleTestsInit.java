package org.mytests.tests;

import com.epam.jdi.light.driver.WebDriverFactory;
import com.epam.jdi.light.driver.get.DriverData;
import org.mytests.uiobjects.site.AutomationPracticeSite;
import org.openqa.selenium.Capabilities;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.*;

import java.io.File;
import java.util.HashMap;

import static com.epam.jdi.light.common.Exceptions.exception;
import static com.epam.jdi.light.driver.WebDriverUtils.killAllSeleniumDrivers;
import static com.epam.jdi.light.driver.get.DriverData.DOWNLOADS_DIR;
import static com.epam.jdi.light.driver.get.DriverData.PAGE_LOAD_STRATEGY;
import static com.epam.jdi.light.logger.LogLevels.STEP;
import static com.epam.jdi.light.settings.WebSettings.logger;
import static com.epam.jdi.light.ui.html.PageFactory.initElements;
import static org.mytests.uiobjects.site.AutomationPracticeSite.homePage;

public interface SimpleTestsInit {
    @BeforeMethod(alwaysRun = true)
    default void openPage(){
        homePage.open();
    }
    @BeforeSuite(alwaysRun = true)
    default void setUp() {
        logger.setLogLevel(STEP);
        DriverData.CHROME_OPTIONS = this::customChromeOptions;
        initElements(AutomationPracticeSite.class);
        logger.info("Run Tests");

    }
    @AfterSuite(alwaysRun = true)
    default void teardown() {
        killAllSeleniumDrivers();
    }
    default Capabilities customChromeOptions() {
        try {
            HashMap<String, Object> chromePrefs = new HashMap<>();
            new File(DOWNLOADS_DIR).mkdirs();
            chromePrefs.put("download.default_directory", DOWNLOADS_DIR);
            ChromeOptions cap = new ChromeOptions();
            cap.setPageLoadStrategy(PAGE_LOAD_STRATEGY);
            cap.addArguments("--start-maximized");
            cap.setExperimentalOption("prefs", chromePrefs);
            return cap;
        } catch (Throwable ex) {
            throw exception("Failed Init Chrome Driver settings: " + ex.getMessage());
        }
    }
}

