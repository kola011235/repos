package org.mytests.uiobjects.site.pages;

import com.epam.jdi.light.elements.base.UIElement;
import com.epam.jdi.light.elements.composite.WebPage;
import com.epam.jdi.light.elements.pageobjects.annotations.FindBy;
import com.epam.jdi.light.elements.pageobjects.annotations.Title;
import com.epam.jdi.light.elements.pageobjects.annotations.Url;
import com.epam.jdi.light.elements.pageobjects.annotations.WaitTime;
import com.epam.jdi.light.elements.pageobjects.annotations.simple.Css;
import com.epam.jdi.light.ui.html.common.Button;
import com.epam.jdi.light.ui.html.common.TextField;

@Url("index.php?controller=order") @Title("Account Page")
public class CartPage extends WebPage {

    @Css("#product_price_5_19_0 > span.price.special-price") public static UIElement cartProductPrice;
    @Css("#total_shipping") public static UIElement shipmentPriceElement;
    @Css("#total_price") public static UIElement checkPriceElement;
    @Css("#\\35 _19_0_0") public static Button trashButton;
    @Css("#center_column > p") public static UIElement errorMessage;
    //@FindBy(name = "quantity_5_19_0_0_hidden")public static TextField quantity;
    @Css("#product_5_19_0_0 > td.cart_quantity.text-center > input.cart_quantity_input.form-control.grey") public static TextField quantity;
    @Css("#cart_quantity_up_5_19_0_0") public static Button plusButton;
    @Css("#cart_quantity_down_5_19_0_0") public static Button minusButton;
    @Css("#product_5_20_0_0 > td.cart_description > small:nth-child(3) > a") public static UIElement colorInfo;

}
