package org.mytests.uiobjects.site.pages;

import com.epam.jdi.light.elements.base.UIElement;
import com.epam.jdi.light.elements.composite.WebPage;
import com.epam.jdi.light.elements.pageobjects.annotations.FindBy;
import com.epam.jdi.light.elements.pageobjects.annotations.Title;
import com.epam.jdi.light.elements.pageobjects.annotations.Url;
import com.epam.jdi.light.elements.pageobjects.annotations.simple.Css;
import com.epam.jdi.light.ui.html.common.Button;
import com.epam.jdi.light.ui.html.common.TextField;

@Url("index.php?controller=authentication&back=my-account") @Title("Login Page")
public class LoginPage extends WebPage {
    @FindBy(id = "email") public static TextField userName;
    @FindBy(id = "passwd")  public static TextField password;
    @FindBy(id = "SubmitLogin") public static Button enter;
    @Css("#center_column > div.alert.alert-danger > ol > li") public static UIElement firstPartOfAlarmMessage;
}
