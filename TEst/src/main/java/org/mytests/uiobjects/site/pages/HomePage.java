package org.mytests.uiobjects.site.pages;

import com.epam.jdi.light.elements.base.UIElement;
import com.epam.jdi.light.elements.composite.WebPage;
import com.epam.jdi.light.elements.pageobjects.annotations.FindBy;
import com.epam.jdi.light.elements.pageobjects.annotations.Title;
import com.epam.jdi.light.elements.pageobjects.annotations.Url;
import com.epam.jdi.light.elements.pageobjects.annotations.simple.Css;
import com.epam.jdi.light.ui.html.common.Button;
import com.epam.jdi.light.ui.html.common.TextField;

import java.util.List;

@Url("index.php") @Title("Home Page")
public class HomePage extends WebPage {
    @FindBy(className = "login") public static Button loginButton;
    @FindBy(name = "search_query") public static TextField searchField;
    @FindBy(name = "submit_search") public static Button searchButton;
    @Css("#header > div:nth-child(3) > div > div > div:nth-child(3) > div > a") public static UIElement cartButton;
    @FindBy(className = "product-name" ) public static List<UIElement> productNames;
    @FindBy(className = "right-block") public static List<UIElement> productDescriptions;
    @FindBy(className = "button-container") public static Button addToCartButton;
    @Css("span[class='continue btn btn-default button exclusive-medium']") public static Button continueShopping;
    @Css("#center_column > ul > li:nth-child(1) > div > div.left-block > div > div.content_price > span.price.product-price") public static UIElement productPriceElement;
    @Css("#center_column > p") public static UIElement searchMessage;
    @Css("#center_column > ul > li:nth-child(1) > div > div.right-block > div.button-container > a.button.lnk_view.btn.btn-default > span") public static Button moreButton;
}
