package org.mytests.uiobjects.site.pages;

import com.epam.jdi.light.elements.composite.WebPage;
import com.epam.jdi.light.elements.pageobjects.annotations.FindBy;
import com.epam.jdi.light.elements.pageobjects.annotations.Title;
import com.epam.jdi.light.elements.pageobjects.annotations.Url;
import com.epam.jdi.light.ui.html.common.Button;
import com.epam.jdi.light.ui.html.common.TextField;

@Url("index.php?controller=my-account") @Title("Account Page")
public class AccountPage extends WebPage {

    @FindBy(className = "account") public static Button buttonWithName;

}
