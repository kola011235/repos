package org.mytests.uiobjects.site;

import com.epam.jdi.light.elements.pageobjects.annotations.JSite;
import org.mytests.uiobjects.site.pages.*;
import org.mytests.uiobjects.site.pages.*;

@JSite("http://automationpractice.com")
public class AutomationPracticeSite {
    public static HomePage homePage;
    public static LoginPage loginPage;
    public static AccountPage accountPage;
    public static CartPage cartPage;
    public static SummerDressPage summerDressPage;
}
