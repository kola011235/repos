package org.mytests.uiobjects.site.pages;

import com.epam.jdi.light.elements.composite.WebPage;
import com.epam.jdi.light.elements.pageobjects.annotations.FindBy;
import com.epam.jdi.light.elements.pageobjects.annotations.Title;
import com.epam.jdi.light.elements.pageobjects.annotations.Url;
import com.epam.jdi.light.elements.pageobjects.annotations.simple.Css;
import com.epam.jdi.light.ui.html.common.Button;
import com.epam.jdi.light.ui.html.common.TextField;

@Url("index.php?controller=my-account") @Title("Account Page")
public class SummerDressPage extends WebPage {

    @Css("#add_to_cart > button > span") public static Button addToCartSummerDressButton;
    @Css("span[class='continue btn btn-default button exclusive-medium']") public static Button continueShoppingSummerDressButton;
    @Css("#quantity_wanted") public static TextField dressQuantity;
    @Css("#color_14") public static Button blueColorButton;
}
