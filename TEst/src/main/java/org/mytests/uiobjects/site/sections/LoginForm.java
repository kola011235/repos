package org.mytests.uiobjects.site.sections;

import com.epam.jdi.light.elements.composite.Form;
import com.epam.jdi.light.elements.pageobjects.annotations.FindBy;
import com.epam.jdi.light.ui.html.common.Button;
import com.epam.jdi.light.ui.html.common.TextField;
import org.mytests.uiobjects.entities.User;

public class LoginForm extends Form<User> {
    @FindBy(id = "email") TextField userName;
    @FindBy(id = "passwd")  TextField password;
    @FindBy(id = "SubmitLogin") Button enter;
}
