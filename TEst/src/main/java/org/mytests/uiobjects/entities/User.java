package org.mytests.uiobjects.entities;


import com.epam.jdi.tools.DataClass;

public class User extends DataClass<User> {
    public String userName;
    public String password;
    public String name;
    public String surname;
}
